// LcdBezel1602.scad
// Bezel assembly for a standard 1602 LCD (V2)
//
// Hint: Select the rendered objects at end of file.
//
// Werner Panocha, January 2022

$fn=36;

// Dimensions

  pcbx = 80;  // PCB
  pcby = 36;
  pcbz = 1.5;
  
  // PCB Mounting hole displaxement
  hx = 75;    
  hy = 31;
  
  
  // Outer hull of LCD frame on the  PCB
  // Notice: this dimensions are used to punch out the room required
  // in the bezel.
  ohx = 71 + 1;       
  ohy = 24 + 2;  
  ohz = 7.15 + .05;  

  // Offset of the Display, relative to center of mounting holes.
  // Horizontally centered, but vertically shifted 1mm downwards
  hox = 0; // 4.25;  // 4.6;  
  hoy = -1.9 / 2; // 5.2;
  
  vpx = 64;   // Viewport (centric to hull)
  vpy = 15;
  vpz = 4;
  
  sax = 41.5; // Solder area
  say = 3.5;
  saz = 2;

 


// -----------------------------------------------------------
// -----------------------------------------------------------
module roundedCube(x,y,z,r){

linear_extrude(height = z) 
union() {
	translate([0+r,0+r,0]) circle(r);
	translate([x-r,0+r,0]) circle(r);
	translate([x-r,y-r,0]) circle(r);
	translate([0+r,y-r,0]) circle(r);

	translate([r,0,0]) square([x-2*r,y]);
	translate([0,r,0]) square([x,y-2*r]);
	}
}

// -----------------------------------------------------------
// -----------------------------------------------------------
module holes(x,y,z,d) {
  translate([0,0,0]) cylinder(h=z, r = d/2);
  translate([x,0,0]) cylinder(h=z, r = d/2);
  translate([x,y,0]) cylinder(h=z, r = d/2);
  translate([0,y,0]) cylinder(h=z, r = d/2);
}
// -----------------------------------------------------------
// -----------------------------------------------------------
module roundedCube(x,y,z,r){

linear_extrude(height = z) 
union() {
	translate([0+r,0+r,0]) circle(r);
	translate([x-r,0+r,0]) circle(r);
	translate([x-r,y-r,0]) circle(r);
	translate([0+r,y-r,0]) circle(r);

	translate([r,0,0]) square([x-2*r,y]);
	translate([0,r,0]) square([x,y-2*r]);
	}
}

// -----------------------------------------------------------
// -----------------------------------------------------------
module holes(x,y,z,d) {
  translate([0,0,0]) cylinder(h=z, r = d/2);
  translate([x,0,0]) cylinder(h=z, r = d/2);
  translate([x,y,0]) cylinder(h=z, r = d/2);
  translate([0,y,0]) cylinder(h=z, r = d/2);
}


// -----------------------------------------------------------
// -----------------------------------------------------------
module lcdHoles(z, d){
  translate([(pcbx - hx )/2, (pcby - hy) /2, 0]) holes(hx, hy, z, d);
}

// -----------------------------------------------------------
// LCD module body
// -----------------------------------------------------------
// 'punch' is used to extend the viewport for punching in bezel
module lcdBody(punch){
  
    difference(){
        union(){
            // pcb
            color("green") cube([pcbx, pcby, pcbz]);                
            
            // connector pins area (room at solder side)
            translate([5.5, pcby-say, pcbz]) 
            color("gold") cube([sax, say, saz]);  
            
            // outer hull of display (tin box)
            // not centric to mounting holes
            xo = ((pcbx - ohx) / 2) + hox;
            yo = ((pcby - ohy) / 2) + hoy;
            translate([xo, yo, pcbz]) {
                
                if(punch){
                    // viewport extended for punching
                    //color("grey") 
                    cube([ohx, ohy, ohz]);
                    translate([(ohx-vpx)/2, (ohy-vpy)/2, ohz-.1])
                    //color("blue")
                    roundedCube(vpx,vpy,vpz,1);             
                }
                else {
                    difference(){
                        color("grey") cube([ohx, ohy, ohz]);
                        
                        // viewport
                        translate([(ohx-vpx)/2, (ohy-vpy)/2, ohz-1])  
                            color("blue") roundedCube(vpx,vpy,vpz,1);                   
                    }
                }
            }
        }
        // Mounting holes
        translate([0,0,-1]) lcdHoles(4, 3.5);          
    }
}


// -----------------------------------------------------------
// Bezel
// -----------------------------------------------------------
module bezel(){
  n1 = 5;
  n2 = 2.5;
  difference(){
    union(){
      roundedCube(pcbx + 2*n1,pcby+2*n1,1,2);
      translate([n2,n2,0]) roundedCube(pcbx + 2*n2,pcby+2*n2,3,1);
      translate([n1,n1, 0])  lcdHoles(ohz + 1 - .01, 7);   // mounting stands
    }
    translate([n1,pcby + n1, ohz + pcbz + 1]) rotate([180,0,0]) lcdBody(true); 
    translate([n1,n1,2])  lcdHoles(ohz + 1, 2.8); 
  }
}

// -----------------------------------------------------------
// Bezel Clamp
// -----------------------------------------------------------
module bezelClamp(){
  
  y = pcby + 2;
  
  difference(){
    union(){
     cube([13, y, 2]);
     translate([13-2,0,0]) cube([2,y,2+6.5]);
    }
    
    translate([3.5, (y - hy) / 2, -.1]) {
      cylinder(d=3.5, h=10);
      translate([0,hy,0]) cylinder(d=3.5, h=10);
    }
    
    translate([-.1, 8,-.1]) cube([10, y- 2* 8, 5]);
    
    translate([10, 8, 6]) cube([10, y- 2* 8, 5]);
  }
  
}



// -----------------------------------------------------------
// Show parts assembly
// -----------------------------------------------------------
module assembly(board){
    
    //  The bezel
    bezel();
    
    
    // If we want to see the board
    if(board){
        translate([5,pcby+5, 25])  rotate([180,0,0]) lcdBody(false);
    }
    // The clamps
    translate([0,0,35]){
        translate([11,4,0]) rotate([0,180,0])  bezelClamp();
        translate([4+hx,42,0]) rotate([0,180,180])  bezelClamp();
    }
}

// -----------------------------------------------------------
// A pair of clamps
// -----------------------------------------------------------
module clamps(){
    translate([4,0,0])  rotate([0,0,-90]) bezelClamp();
    translate([4 + 44,0,0])  rotate([0,0,-90]) bezelClamp(); 
}
// -----------------------------------------------------------
// Parts to print a kit
// -----------------------------------------------------------
module kit(){
    
    bezel();
    translate([0,62,0]) clamps();
}


// -----------------------------------------------------------
// Select part to be rendered
// -----------------------------------------------------------
*lcdBody(false);
assembly(true);
*bezel();
*bezelClamp();
*clamps();
*kit();

// eof
# Mounting Frame for a Standard 1602 LCD Module 
designed with [OpenSCAD](https://openscad.org/)

Requires apprx. 41 x 85 mm breakthrough in frontpanel etc..  
The intention of the bezel is to coat any inaccurate handcrafted breakthrough.  
If you want to play safe, take the actual measurements from your printed part.  


![Assembled view 1](./png/LCD1602-assembly1.png) 

![Assembled view 2](./png/LCD1602-assembly2.png) 

### STL Files

**[Complete kit](./stl/LCD1602-kit.stl)** (complete mounting kit, bezel and clamps)  
**[Bezel](./stl/LCD1602-bezel.stl)** (only bezel)  
**[Clamps](./stl/LCD1602-clamps.stl)** (2 mounting clamps)  

### License   

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <span resource="[_:publisher]" rel="dct:publisher">
    <span property="dct:title">the author</span></span>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">this repository</span>.
<br>This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="DE" about="[_:publisher]">
  Germany</span>.
</p>